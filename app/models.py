import md5
import re

from app import app, db
from datetime import datetime

ROLE_USER = 0
ROLE_ADMIN = 1

__table_args__ = {'extend_existing': True}


def hash_pass(password):
        """
            Return the md5 hash of the password+salt
        """
        salted_password = str(password) + str(app.secret_key)
        return md5.new(salted_password).hexdigest()


slug_re = re.compile('[a-zA-Z0-9]+')


def slugify(title):
    title = title.lower()
    _title = title[:99].replace(' ', '-')  # Changed slug length to 100
    return '-'.join(re.findall(slug_re, _title))


class Users(db.Model):
    __tablename__ = 'Users'
    id = db.Column('user_id', db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    firstname = db.Column('firstname', db.String(80))
    lastname = db.Column('lastname', db.String(80))
    is_active = db.Column(db.Boolean, default=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    date_joined = db.Column(db.DateTime)

    def __init__(self, username, email, passwd):
        self.username = username
        self.email = email
        self.date_joined = datetime.utcnow()
        self.password = hash_pass(passwd)

    def get(userid):
        """
        Static method to search the database and see if userid exists.  If it
        does exist then return a User Object.  If not then return None as
        required by Flask-Login.
        """
        # For this example the USERS database is a list consisting of
        #(user,hased_password) of users.
        for user in self.query.all():
            print "bitch"

    def is_authenticated(self):
        """ Return a boolean

        Check if the user is authenticated

        """
        return True

    def is_active(self):
        """ Return a boolean

        Check if the is active (account not deactivated or is not banned)

        """
        return True

    def is_anonymous(self):
        """ Return a boolean

        Check if this is an anonymous user. The actual user will return False.

        """
        return False

    def get_id(self):
        """ Return a unicode

        Returns an unique user identifier.
        Seen that the id is an integer, it's converted to a unicode

        """
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % self.username


class Posts(db.Model):
    __tablename__ = 'Posts'
    id = db.Column('post_id', db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True)
    content = db.Column(db.Text)
    tags = db.Column(db.String(20))
    permalink = db.Column(db.String(255))
    created = db.Column(db.DateTime)
    last_modified = db.Column(db.DateTime)
    # status = db.Column(db.String)
    author_id = db.Column(db.Integer, db.ForeignKey('Users.user_id'))
    author = db.relationship(Users, backref='posts')
    category_id = db.Column(db.Integer, db.ForeignKey('Category.category_id'))
    category = db.relationship('Category', backref=db.backref('posts', lazy='dynamic'))

    def __init__(self, title, content, tags, category):
        self.title = title
        self.content = content
        self.created = datetime.utcnow()
        self.tags = tags
        self.permalink = slugify(title)
        self.category = category

    def __unicode__(self):
        return self.slug


class Comments(db.Model):
    __tablename__ = 'Comments'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    author = db.Column(db.String(80), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    content = db.Column(db.Text, nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey(Posts.id), nullable=False)
    post = db.relationship(Posts, backref='comments')


class Category(db.Model):
    __tablename__ = 'Category'
    id = db.Column('category_id', db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    permalink = db.Column(db.String(255), nullable=False)

    def __init__(self, title):
        self.title = title
        self.permalink = slugify(title)

    def __unicode__(self):
        return self.title
