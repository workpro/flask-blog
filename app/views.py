import md5

from flask import request, render_template, redirect, url_for, g
from flask.ext.login import login_user, logout_user, current_user, login_required, flash

from app import app, db, login_manager as lm
from models import Users, Posts, hash_pass
from forms import LoginForm


@app.errorhandler(404)
def _404(e):
    """
        Web page to display when a 404 error (Page not found) is handled
    """
    return render_template('errors/404.html'), 404


@app.errorhandler(500)
def _500(e):
    """
        Web page to display when a 500 error (Internal Server Error) is handled
    """
    return render_template('errors/500.html'), 404

#########################################
#            FRONTEND VIEWS             #
#########################################


@app.route('/index', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
def index():
    """
        Return the 6 lasts posts, ordered from most recently posted to older.
    """
    posts = Posts.query.order_by('created DESC').limit(5)
    return render_template("index.html", title='Blog', posts=posts)


@app.route('/<permalink>')
@app.route('/<permalink>/')
def single_post(permalink):
    """
        Return the matching article if exists.
        If not, a 404 error is handled
    """
    matching_post = Posts.query.filter_by(permalink=permalink).first_or_404()
    return render_template("single.html", post=matching_post)


@app.route('/authors/<author_id>')
@app.route('/authors/<author_id>/')
def author(author_id):
    posts = Posts.query.filter_by(author_id=author_id)
    title = "Posts by " + Users.query.get(author_id).firstname + " " + Users.query.get(author_id).lastname
    return render_template("posts_by_author.html",
                           title=title,
                           posts=posts)


########################################
#            BACKEND VIEWS             #
########################################

@lm.user_loader
def load_user(id):
    return Users.query.get(int(id))


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
        Web page to display Login Form and process form
    """
    form = LoginForm()          # Loading the login form
    error = None
    if request.method == "POST":
        # Set the username and the password with the submitted
        # username/password and try to get a user matching this
        # username in the database
        username = request.form['username']
        password = request.form['password']
        user = Users.query.filter_by(username=username).first()

        # If there is a user with this username then check that the submitted
        # password matches the password in the database.  The password is stored
        # in an hash format, so you must hash the submitted password before comparing
        # it.
        if user:
            if user.is_active:
                if hash_pass(request.form['password']) == user.password:
                    login_user(user)
                    flash("Logged in successfully.")
                    g.user = user
                    return redirect(request.args.get("next") or url_for("admin"))
                    # return redirect("http://google.fr")
                else:
                    error = "Username or Password Incorrect"
            else:
                error = "Yess"
        else:
            error = "No user found with this username"

    return render_template("login.html",
                           form=form,
                           error=error,
                           title="Sign In")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("admin"))


@app.route('/admin')
@app.route('/admin/')
@login_required
def admin():
    return render_template("backend/index.html", title='Backend')


@app.route('/admin/blog')
@app.route('/admin/blog/')
@login_required
def backend_blog():
    return render_template("backend/blog.html", title='Blog')


@app.route('/admin/projects')
@app.route('/admin/projects/')
@login_required
def backend_projects():
    return render_template("backend/projects.html", title='Projects')


@app.route('/admin/events')
@app.route('/admin/events/')
@login_required
def backend_events():
    return render_template("backend/events.html", title='Events')


@app.route('/admin/users')
@app.route('/admin/users/')
@login_required
def backend_users():
    return render_template("backend/users.html", title='Users Management')


@app.route('/admin/stats')
@app.route('/admin/stats/')
@login_required
def backend_stats():
    return render_template("backend/stats.html", title='Statistics')
